<?php

return [
    '@uploads' => '{{ uploads_basepath }}',

    '@hostName' => 'http://{{ server_hostname }}',
    '@uploadsUrl' => 'http://{{ server_hostname }}',

];