#!/bin/sh

apt-get -y update
apt-get -y install software-properties-common python-software-properties
dpkg-reconfigure python-software-properties
apt-add-repository -y ppa:ansible/ansible
apt-get -y update
apt-get -y install ansible

ansible-playbook ./playbook.yml -i ./inventory --connection=local
