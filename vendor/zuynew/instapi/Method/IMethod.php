<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 2:49 PM
 */

namespace zuynew\instapi\Method;


use zuynew\instapi\Client\IClient;

interface IMethod {

    public function execute(IClient $client, array $data = [], array $params = []);

}