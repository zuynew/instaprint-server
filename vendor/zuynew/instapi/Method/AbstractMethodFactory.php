<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 2:41 PM
 */

namespace zuynew\instapi\Method;


abstract class AbstractMethodFactory {

    abstract public function getCreateSubscriptionMethod();

    abstract public function getListSubscriptionsMethod();

    abstract public function getDeleteSubscriptionMethod();

    abstract public function getListMediaTagMethod();

} 