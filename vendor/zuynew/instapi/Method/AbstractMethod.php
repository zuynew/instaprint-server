<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 2:50 PM
 */

namespace zuynew\instapi\Method;


use zuynew\instapi\Client\IClient;
use zuynew\instapi\Exception\ApiError;
use zuynew\instapi\Exception\InstapiException;
use zuynew\instapi\Request;

abstract class AbstractMethod implements IMethod {

    protected $_requestClass = 'zuynew\instapi\Request\Request';

    protected $_url = 'https://api.instagram.com/v1/';

    protected $_verb = 'GET';

    protected $_form = [];

    protected function _buildRequest (Request\IRequest $request, IClient $client, array $data = [], array $params = [])
    {
        $request
            ->setUrl($this->_url)
            ->setVerb($this->_verb)
            ->setData(array_merge($this->_form, $data))
            ->setCredentials($client)
            ->setParams($params)
        ;

        return $request;

    }

    public function execute(IClient $client, array $data = [], array $params = [])
    {
        $res = json_decode($this->_buildRequest(new $this->_requestClass(), $client, $data, $params)->execute(), true);

        if (isset($res['meta']) && isset($res['meta']['error_type'])) {
            throw new ApiError($res['meta']);
        } elseif (!isset($res['meta'])) {
            throw new InstapiException("Undefined error");
        }

        return $res['data'];

    }

}