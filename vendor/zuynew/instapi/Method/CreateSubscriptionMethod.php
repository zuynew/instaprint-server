<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 3:37 PM
 */

namespace zuynew\instapi\Method;


use zuynew\instapi\Client\IClient;
use zuynew\instapi\Request\Request;

class CreateSubscriptionMethod extends  AbstractMethod {

    protected $_url = 'https://api.instagram.com/v1/subscriptions/';

    protected $_verb = 'POST';

    protected $_form = ['object' => 'tag', 'aspect' => 'media', 'callback_url' => ''];

}