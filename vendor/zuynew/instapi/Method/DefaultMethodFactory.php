<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 3:58 PM
 */

namespace zuynew\instapi\Method;


class DefaultMethodFactory extends AbstractMethodFactory {
    public function getCreateSubscriptionMethod()
    {
        return new CreateSubscriptionMethod();
    }

    public function getListSubscriptionsMethod()
    {
        return new ListSubscriptionsMethod();
    }

    public function getDeleteSubscriptionMethod()
    {
        return new DeleteSubscriptionMethod();
    }

    public function getListMediaTagMethod()
    {
        return new ListMediaTagMethod();
    }

    public function getListMediaUserMethod()
    {
        return new ListMediaUserMethod();
    }

    public function getListMediaLocationMethod()
    {
        return new ListMediaLocationMethod();
    }

    public function getListMediaGeographyMethod()
    {
        return new ListMediaGeographyMethod();
    }

} 