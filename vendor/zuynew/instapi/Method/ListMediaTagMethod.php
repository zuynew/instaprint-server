<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 3:37 PM
 */

namespace zuynew\instapi\Method;


use zuynew\instapi\Client\IClient;
use zuynew\instapi\Request\Request;

class ListMediaTagMethod extends  AbstractMethod {

    protected $_url = 'https://api.instagram.com/v1/tags/{object_id}/media/recent';

    protected $_verb = 'GET';

    protected $_form = ['COUNT' => 1000];

}