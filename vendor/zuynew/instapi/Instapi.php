<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 10/31/14
 * Time: 12:59 AM
 */

namespace zuynew\instapi;


use zuynew\instapi\Client\IClient;
use zuynew\instapi\Exception\MethodNotImplementedInstapiException;
use zuynew\instapi\Method\AbstractMethodFactory;

/**
 * Class Instapi
 * @package zuynew\instapi
 *
 * @method createSubscription
 * @method deleteSubscription
 * @method listSubscriptions
 * @method listMediaTag
 * @method listMediaUser
 * @method listMediaLocation
 * @method listMediaGeography
 *
 */
class Instapi {

    protected $_client;

    protected $_methodFactory;

    function __construct(IClient $client, AbstractMethodFactory $methodFactory)
    {
        $this->_client = $client;
        $this->_methodFactory = $methodFactory;
    }

    function __call($name, $arguments)
    {
        $getterName = 'get'.ucfirst($name).'Method';
        if (method_exists($this->_methodFactory, $getterName)) {
            $method = $this->_methodFactory->$getterName();
            return call_user_func_array([$method, 'execute'], array_merge([$this->_client], $arguments));
        }
        throw new MethodNotImplementedInstapiException($name);
    }

}