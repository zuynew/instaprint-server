<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 3:11 PM
 */

namespace zuynew\instapi\Request;


use zuynew\instapi\Client\IClient;

interface IRequest {
    public function setUrl($url);
    public function setData($params);
    public function setParams($params);
    public function setCredentials(IClient $client);
    public function setVerb($verb);
    public function execute();
}