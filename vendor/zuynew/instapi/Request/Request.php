<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 3:16 PM
 */

namespace zuynew\instapi\Request;

use zuynew\instapi\Client\IClient;

class Request implements IRequest {


    protected $_curl;

    protected $_url;

    protected $_verb;

    protected $_data = [];

    function __construct()
    {
        $this->_curl = curl_init();

        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
    }

    public function setUrl($url)
    {
        $this->_url = $url;

        return $this;

    }

    public function setData($params)
    {
        $this->_data = array_merge($this->_data, $params);

        return $this;

    }

    public function setCredentials(IClient $client)
    {
        $this->_data = array_merge($this->_data, ['client_id' => $client->getClientId(), 'client_secret' => $client->getClientSecret()]);

        return $this;

    }

    public function setVerb($verb)
    {
        $this->_verb = $verb;

        return $this;

    }

    public function execute()
    {
        switch ($this->_verb) {
            case "GET":
            case "DELETE":
                curl_setopt($this->_curl, CURLOPT_URL, $this->_url.'?'.http_build_query($this->_data));
                break;
            default:
                curl_setopt($this->_curl, CURLOPT_URL, $this->_url);
                curl_setopt($this->_curl, CURLOPT_POSTFIELDS, $this->_data);
        }

        curl_setopt($this->_curl, CURLOPT_CUSTOMREQUEST, $this->_verb);

        return curl_exec($this->_curl);

    }

    function __destruct()
    {
        curl_close($this->_curl);
    }

    public function setParams($params)
    {
        foreach ($params as $key => $value) {
            $this->_url = preg_replace('/{'.$key.'}/', $value, $this->_url);
        }
        return $this;
    }


} 