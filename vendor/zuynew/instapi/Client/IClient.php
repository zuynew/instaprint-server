<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 2:33 PM
 */

namespace zuynew\instapi\Client;


interface IClient {

    public function getClientId();

    public function getClientSecret();

} 