<?php

use yii\db\Schema;
use yii\db\Migration;

class m141027_185513_create_tag_table extends Migration
{
    public function up()
    {
        $this->execute('
        CREATE TABLE `tag` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(255) NULL,
            PRIMARY KEY (`id`),
            UNIQUE INDEX `name_UNIQUE` (`name` ASC));
        ');
    }

    public function down()
    {
        echo "m141027_185513_create_tag_table cannot be reverted.\n";

        return false;
    }
}
