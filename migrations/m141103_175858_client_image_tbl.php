<?php

use yii\db\Schema;
use yii\db\Migration;

class m141103_175858_client_image_tbl extends Migration
{
    public function up()
    {
        $this->execute('
        CREATE TABLE `client_image` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `client_id` INT NOT NULL,
            `image_id` INT NOT NULL,
            PRIMARY KEY (`id`),
            INDEX `fk_client_image_1_idx` (`client_id` ASC),
            INDEX `fk_client_image_2_idx` (`image_id` ASC),
            CONSTRAINT `fk_client_image_1`
                FOREIGN KEY (`client_id`)
                REFERENCES `client` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
            CONSTRAINT `fk_client_image_2`
                FOREIGN KEY (`image_id`)
                REFERENCES `image` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION);
        ');
    }

    public function down()
    {
        echo "m141103_175858_client_image_tbl cannot be reverted.\n";

        return false;
    }
}
