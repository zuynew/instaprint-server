<?php

use yii\db\Schema;
use yii\db\Migration;

class m141102_155350_image_fk extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `image`
            DROP FOREIGN KEY `fk_image_1`;
            ALTER TABLE `image`
            CHANGE COLUMN `tag_id` `subscription_id` INT(11) NOT NULL ;
            ALTER TABLE `image`
            ADD CONSTRAINT `fk_image_1`
              FOREIGN KEY (`subscription_id`)
              REFERENCES `subscription` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');
    }

    public function down()
    {
        echo "m141102_155350_image_fk cannot be reverted.\n";

        return false;
    }
}
