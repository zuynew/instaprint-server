<?php

use yii\db\Schema;
use yii\db\Migration;

class m141102_190530_subscr_indexes extends Migration
{
    public function up()
    {
        $this->execute("
        ALTER TABLE `subscription`
            CHANGE COLUMN `object_id` `object_id` VARCHAR(255) NOT NULL ,
            CHANGE COLUMN `verify_token` `verify_token` VARCHAR(45) NULL ,
            CHANGE COLUMN `subscription_external_id` `subscription_external_id` INT(11) NULL ;
            CHANGE COLUMN `type` `type` ENUM('tag','user','location','geography') NOT NULL ;
        ");
    }

    public function down()
    {
        echo "m141102_190530_subscr_indexes cannot be reverted.\n";

        return false;
    }
}
