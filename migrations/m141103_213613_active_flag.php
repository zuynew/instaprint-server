<?php

use yii\db\Schema;
use yii\db\Migration;

class m141103_213613_active_flag extends Migration
{
    public function up()
    {
        $this->execute('
            ALTER TABLE `subscription`
              ADD COLUMN `active` TINYINT(1) NOT NULL DEFAULT 1 AFTER `type`;
        ');
    }

    public function down()
    {
        echo "m141103_213613_active_flag cannot be reverted.\n";

        return false;
    }
}
