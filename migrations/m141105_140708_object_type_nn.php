<?php

use yii\db\Schema;
use yii\db\Migration;

class m141105_140708_object_type_nn extends Migration
{
    public function up()
    {
        $this->execute("
        ALTER TABLE `subscription`
          CHANGE COLUMN `type` `type` ENUM('tag','user','location','geography') NOT NULL ;
        ");
    }

    public function down()
    {
        echo "m141105_140708_object_type_nn cannot be reverted.\n";

        return false;
    }
}
