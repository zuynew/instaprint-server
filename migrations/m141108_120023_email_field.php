<?php

use yii\db\Schema;
use yii\db\Migration;

class m141108_120023_email_field extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `client`
            ADD COLUMN `email` VARCHAR(155) NOT NULL AFTER `id`,
            ADD UNIQUE INDEX `email_UNIQUE` (`email` ASC),
            DROP INDEX `name_UNIQUE` ;
        ');
    }

    public function down()
    {
        echo "m141108_120023_email_field cannot be reverted.\n";

        return false;
    }
}
