<?php

use yii\db\Schema;
use yii\db\Migration;

class m141102_155228_client_subscription_fk extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `client_subscription`
            ADD CONSTRAINT `fk_client_subscription_1`
              FOREIGN KEY (`client_id`)
              REFERENCES `client` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION,
            ADD CONSTRAINT `fk_client_subscription_2`
              FOREIGN KEY (`subscription_id`)
              REFERENCES `subscription` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;
        ');
    }

    public function down()
    {
        echo "m141102_155228_client_subscription_fk cannot be reverted.\n";

        return false;
    }
}
