<?php

use yii\db\Schema;
use yii\db\Migration;

class m141103_191417_tags_links_fields extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `client_subscription`
            ADD COLUMN `tags` VARCHAR(255) NULL AFTER `end_date`,
            ADD COLUMN `links` VARCHAR(255) NULL AFTER `tags`;
        ');
    }

    public function down()
    {
        echo "m141103_191417_tags_links_fields cannot be reverted.\n";

        return false;
    }
}
