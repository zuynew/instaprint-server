<?php

use yii\db\Schema;
use yii\db\Migration;

class m141102_154004_tag_to_subscription extends Migration
{
    public function up()
    {
        $this->execute("
        ALTER TABLE `tag`
            CHANGE COLUMN `name` `object_id` VARCHAR(255) NULL DEFAULT NULL ,
            CHANGE COLUMN `subscription_id` `subscription_external_id` INT(11) NOT NULL ,
            ADD COLUMN `type` ENUM('tag', 'user', 'location', 'geography') NULL AFTER `subscription_external_id`, RENAME TO  `subscription` ;
        ");
    }

    public function down()
    {
        echo "m141102_154004_tag_to_subscription cannot be reverted.\n";

        return false;
    }
}
