<?php

use yii\db\Schema;
use yii\db\Migration;

class m141103_172403_username_field extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `image`
          ADD COLUMN `username` VARCHAR(150) NOT NULL AFTER `subscription_id`;
        ');
    }

    public function down()
    {
        echo "m141103_172403_username_field cannot be reverted.\n";

        return false;
    }
}
