<?php

use yii\db\Schema;
use yii\db\Migration;

class m141104_105651_watermark_field extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `client_subscription`
          ADD COLUMN `watermark` VARCHAR(255) NULL AFTER `links`;
        ');
    }

    public function down()
    {
        echo "m141104_105651_watermark_field cannot be reverted.\n";

        return false;
    }
}
