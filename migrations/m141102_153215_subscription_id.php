<?php

use yii\db\Schema;
use yii\db\Migration;

class m141102_153215_subscription_id extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `tag`
          ADD COLUMN `subscription_id` INT NOT NULL AFTER `verify_token`;
        ');
    }

    public function down()
    {
        echo "m141102_153215_subscription_id cannot be reverted.\n";

        return false;
    }
}
