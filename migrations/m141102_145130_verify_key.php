<?php

use yii\db\Schema;
use yii\db\Migration;

class m141102_145130_verify_key extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `tag`
          ADD COLUMN `verify_token` VARCHAR(45) NOT NULL AFTER `name`;
        ');
    }

    public function down()
    {
        echo "m141102_145130_verify_key cannot be reverted.\n";

        return false;
    }
}
