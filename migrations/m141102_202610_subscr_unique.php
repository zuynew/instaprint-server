<?php

use yii\db\Schema;
use yii\db\Migration;

class m141102_202610_subscr_unique extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `subscription`
            ADD UNIQUE INDEX `index2` (`object_id` ASC, `type` ASC),
            DROP INDEX `name_UNIQUE` ;
        ');
    }

    public function down()
    {
        echo "m141102_202610_subscr_unique cannot be reverted.\n";

        return false;
    }
}
