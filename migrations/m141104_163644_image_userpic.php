<?php

use yii\db\Schema;
use yii\db\Migration;

class m141104_163644_image_userpic extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `image`
          ADD COLUMN `userpic_path` VARCHAR(255) NULL AFTER `username`;
        ');
    }

    public function down()
    {
        echo "m141104_163644_image_userpic cannot be reverted.\n";

        return false;
    }
}
