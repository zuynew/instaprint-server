<?php

use yii\db\Schema;
use yii\db\Migration;

class m141027_184717_create_client_table extends Migration
{
    public function up()
    {
        $this->execute('
        CREATE TABLE `client` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(255) NULL,
            `api_key` VARCHAR(255) NULL,
            PRIMARY KEY (`id`));
        ');
    }

    public function down()
    {
        echo "m141027_184717_create_client_table cannot be reverted.\n";

        return false;
    }
}
