<?php

use yii\db\Schema;
use yii\db\Migration;

class m141102_154541_tag_to_subscription_column extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `client_tag`
            DROP FOREIGN KEY `fk_client_tag_2`;
            ALTER TABLE `client_tag`
            CHANGE COLUMN `tag_id` `subscription_id` INT(11) NULL DEFAULT NULL , RENAME TO  `client_subscription` ;
            ALTER TABLE `client_tag`
            ADD CONSTRAINT `fk_client_tag_2`
              FOREIGN KEY (`subscription_id`)
              REFERENCES `tag` (`id`)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION;

        ');
    }

    public function down()
    {
        echo "m141102_154541_tag_to_subscription_column cannot be reverted.\n";

        return false;
    }
}
