<?php

use yii\db\Schema;
use yii\db\Migration;

class m141027_190012_create_client_tag_table extends Migration
{
    public function up()
    {
        $this->execute('
        CREATE TABLE `client_tag` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `client_id` INT NULL,
            `tag_id` INT NULL,
            `start_date` DATETIME NOT NULL,
            `end_date` DATETIME NULL,
            PRIMARY KEY (`id`),
            INDEX `fk_client_tag_1_idx` (`client_id` ASC),
            INDEX `fk_client_tag_2_idx` (`tag_id` ASC),
            CONSTRAINT `fk_client_tag_1`
            FOREIGN KEY (`client_id`)
            REFERENCES `client` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
            CONSTRAINT `fk_client_tag_2`
            FOREIGN KEY (`tag_id`)
            REFERENCES `tag` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION);
        ');
    }

    public function down()
    {
        echo "m141027_190012_create_client_tag_table cannot be reverted.\n";

        return false;
    }
}
