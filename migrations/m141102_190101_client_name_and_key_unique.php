<?php

use yii\db\Schema;
use yii\db\Migration;

class m141102_190101_client_name_and_key_unique extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `client`
            CHANGE COLUMN `name` `name` VARCHAR(255) NOT NULL ,
            CHANGE COLUMN `api_key` `api_key` VARCHAR(255) NOT NULL ,
            ADD UNIQUE INDEX `name_UNIQUE` (`name` ASC),
            ADD UNIQUE INDEX `api_key_UNIQUE` (`api_key` ASC);
        ');
    }

    public function down()
    {
        echo "m141102_190101_client_name_and_key_unique cannot be reverted.\n";

        return false;
    }
}
