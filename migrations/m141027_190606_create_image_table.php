<?php

use yii\db\Schema;
use yii\db\Migration;

class m141027_190606_create_image_table extends Migration
{
    public function up()
    {
        $this->execute('
        CREATE TABLE `image` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `path` VARCHAR(255) NOT NULL,
            `create_at` DATETIME NOT NULL,
            `tag_id` INT NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE INDEX `path_UNIQUE` (`path` ASC),
            INDEX `fk_image_1_idx` (`tag_id` ASC),
            CONSTRAINT `fk_image_1`
            FOREIGN KEY (`tag_id`)
            REFERENCES `tag` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION);
        ');
    }

    public function down()
    {
        echo "m141027_190606_create_image_table cannot be reverted.\n";

        return false;
    }
}
