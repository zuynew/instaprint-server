<?php

use yii\db\Schema;
use yii\db\Migration;

class m141105_140223_users_fields extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `client`
            ADD COLUMN `oauth_access_token` VARCHAR(255) NOT NULL AFTER `api_key`,
            ADD COLUMN `oauth_valid_until` INT NOT NULL AFTER `oauth_access_token`,
            ADD COLUMN `oauth_refresh_token` VARCHAR(255) NOT NULL AFTER `oauth_valid_until`,
            ADD COLUMN `auth_key` VARCHAR(255) NULL AFTER `oauth_refresh_token`;
        ');
    }

    public function down()
    {
        echo "m141105_140223_users_fields cannot be reverted.\n";

        return false;
    }
}
