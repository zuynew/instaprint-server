<?php

use yii\db\Schema;
use yii\db\Migration;

class m141103_165751_nullable_api_key extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `client`
          CHANGE COLUMN `api_key` `api_key` VARCHAR(255) NULL ;
        ');
    }

    public function down()
    {
        echo "m141103_165751_nullable_api_key cannot be reverted.\n";

        return false;
    }
}
