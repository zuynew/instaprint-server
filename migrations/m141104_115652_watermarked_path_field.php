<?php

use yii\db\Schema;
use yii\db\Migration;

class m141104_115652_watermarked_path_field extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `client_image`
          ADD COLUMN `watermarked_path` VARCHAR(255) NULL AFTER `image_id`;
        ');
    }

    public function down()
    {
        echo "m141104_115652_watermarked_path_field cannot be reverted.\n";

        return false;
    }
}
