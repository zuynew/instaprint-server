<?php

use yii\db\Schema;
use yii\db\Migration;

class m141102_173734_create_at_null extends Migration
{
    public function up()
    {
        $this->execute('
        ALTER TABLE `image`
          CHANGE COLUMN `create_at` `created_at` DATETIME NULL ;
        ');
    }

    public function down()
    {
        echo "m141102_173734_create_at_null cannot be reverted.\n";

        return false;
    }
}
