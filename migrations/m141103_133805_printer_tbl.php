<?php

use yii\db\Schema;
use yii\db\Migration;

class m141103_133805_printer_tbl extends Migration
{
    public function up()
    {
        $this->execute('
        CREATE TABLE `printer` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(255) NULL,
            `external_id` VARCHAR(45) NOT NULL,
            `client_id` INT NOT NULL,
            PRIMARY KEY (`id`),
            INDEX `fk_printer_1_idx` (`client_id` ASC),
            CONSTRAINT `fk_printer_1`
                FOREIGN KEY (`client_id`)
                REFERENCES `client` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION);
        ');
    }

    public function down()
    {
        echo "m141103_133805_printer_tbl cannot be reverted.\n";

        return false;
    }
}
