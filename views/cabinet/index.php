<?php

use yii\helpers\Html;
use \yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use \app\models\Subscription;
use \app\models\ClientSubscription;


/* @var $this yii\web\View */
/* @var $clientSubscriptionProvider yii\data\ActiveDataProvider */
/* @var $printerProvider yii\data\ActiveDataProvider */

$this->registerJs(<<<JS
    $('html').on('submit', '#subscription-create', function (e) {
            e.preventDefault();


            var self = $(this)

            var action = (self.attr('action') === undefined) ? '/' : self.attr('action');
            var method = (self.attr('method') === undefined) ? 'post' : self.attr('method');

            console.log(action, method)

            $.ajax(
                {
                    'url': action,
                    'method': method,
                    'data': self.serialize(),
                    'success': function (data) {
                        self[0].reset();
                        var pjax_id = $("#subscriptions-table").attr('id');
                        $.pjax.reload('#' + pjax_id);
                    }
                }
            );

        }
    )
JS
,
yii\web\View::POS_HEAD);

$this->title = 'Cabinet';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cabinet-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <h2>Subscriptions</h2>
    <?php
        $form = ActiveForm::begin(
            [
                'action' => 'subscription/create',
                'layout' => 'horizontal',

                'id' => 'subscription-create',

                'fieldConfig' => [
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-1',
                        'wrapper' => 'col-sm-3',
                        'offset' => 'col-sm-offset-0',
                    ],
                ],
            ]
        );
        $subscription = new Subscription();
        $clientSubscription = new ClientSubscription();
    ?>

    <?= $form->field($subscription, 'object_id')->textInput(['maxlength' => 155]) ?>

    <?= $form
        ->field($subscription, 'type')
        ->dropDownList(
            [
                Subscription::SUBSCRIPTION_TYPE_TAG => 'Tag',
                Subscription::SUBSCRIPTION_TYPE_USER => 'User',
                Subscription::SUBSCRIPTION_TYPE_LOCATION => 'Location',
                Subscription::SUBSCRIPTION_TYPE_GEOGRAPHY => 'Geography'
            ]
        )
    ?>

    <div class="form-group field-clientsubscription-filters">
        <label class="control-label col-sm-1" for="clientsubscription-filters">Filters</label>
        <div class="col-sm-3">
            <?= Html::textarea('ClientSubscription[filters]', '', ['id' => 'clientsubscription-filters', 'placeholder' => 'e.g. #tag, @user', 'class' => 'form-control']) ?>
            <div class="help-block help-block-error "></div>
        </div>

    </div>

    <?= $form->field($clientSubscription, 'start_date')->widget(\yii\jui\DatePicker::className(),['options' => ['class' => 'form-control'], 'dateFormat' => 'yyyy-MM-dd','clientOptions' => [ 'defaultDate' => date('Y-m-d')]])?>

    <?= $form->field($clientSubscription, 'end_date')->widget(\yii\jui\DatePicker::className(), ['options' => ['class' => 'form-control'], 'dateFormat' => 'yyyy-MM-dd'])?>

    <div class="form-group">
        <?= Html::submitButton('Create', ['class' => 'btn btn-success  col-sm-offset-3']) ?>
    </div>

    <?php ActiveForm::end(); ?>



    <?php \yii\widgets\Pjax::begin(['options' => ['id' => 'subscriptions-table', 'class' => 'pjax-wraper']]);?>
    <?= GridView::widget([
            'dataProvider' => $clientSubscriptionProvider,
            'columns' => [
                'subscription.object_id',
                'subscription.type',
                [
                    'attribute' => 'start_date',
                    'enableSorting' => false
                ],
                [
                    'attribute' => 'end_date',
                    'enableSorting' => false
                ],
                [
                    'label' => 'Tags',
                    'format' => 'raw',
                    'value' => function ($model) {
                            return implode(
                                ', ',
                                array_map(
                                    function ($el) {
                                        return '#'.$el;
                                    },
                                    $model->tags
                                )
                            );
                        },
                    'enableSorting' => false
                ],
                [
                    'label' => 'Links',
                    'format' => 'raw',
                    'value' => function ($model) {
                            return implode(
                                ', ',
                                array_map(
                                    function ($el) {
                                        return '@'.$el;
                                    },
                                    $model->links
                                )
                            );
                        },
                    'enableSorting' => false
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'controller' => 'subscription',
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-remove-sign"></span>', $url, [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'class' => 'grid-action',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this printer?'),
                                    'data-method' => 'delete',
                                    'data-pjax' => '0',
                                ]
                            );
                        }
                    ]
                ],
            ],
        ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>

    <?=Html::a('<h2>Printers</h2>', 'cabinet/printers')?>

    <?php \yii\widgets\Pjax::begin(['options' => ['class' => 'pjax-wraper']]);?>
    <?= GridView::widget([
            'dataProvider' => $printerProvider,
            'columns' => [
                [
                    'attribute' => 'name',
                    'enableSorting' => false
                ],
                [
                    'attribute' => 'external_id',
                    'enableSorting' => false
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'controller' => 'printer',
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-remove-sign"></span>', $url, [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'class' => 'grid-action',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this printer?'),
                                    'data-method' => 'delete',
                                    'data-pjax' => '0',
                                    'data-getData' => json_encode(['Printer[external_id]' => $model->external_id])
                                ]
                            );
                        }
                    ]
                ],
            ],
        ]); ?>

    <?php \yii\widgets\Pjax::end(); ?>
</div>