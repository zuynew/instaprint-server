<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $printerProvider yii\data\ActiveDataProvider */

$this->title = 'Printers';
$this->params['breadcrumbs'][] = [
    'label' =>'Cabinet',
    'url' => '/cabinet'
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cabinet-printers">

    <h1><?= Html::encode($this->title) ?></h1>
<?php \yii\widgets\Pjax::begin(['options' => ['class' => 'pjax-wraper']]);?>
<?= GridView::widget([
        'dataProvider' => $printerProvider,
        'columns' => [
            'name',
            'id',
            'status',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{append}{remove}{delete}',
                'buttons' => [
                    'append' => function ($url, $model, $key) {
                        $a = '';
                        if ($model['status'] == \app\models\Printer::PRINTER_STATUS_AVAILABLE) {
                            $a = Html::a('<span class="glyphicon glyphicon-plus-sign"></span>', $url, [
                                    'title' => Yii::t('yii', 'Append'),
                                    'class' => 'grid-action',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to append this printer?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                    'data-postData' =>
                                        json_encode(
                                            [
                                                'Printer[external_id]' => $model['id'],
                                                'Printer[name]' => $model['name']
                                            ]
                                        )
                                ]
                            );
                        }
                        return $a;
                    },

                    'remove' => function ($url, $model, $key) {
                        $a = '';
                        if ($model['status'] == \app\models\Printer::PRINTER_STATUS_ACTIVE) {
                            $a = Html::a('<span class="glyphicon glyphicon-minus-sign"></span>', $url, [
                                    'title' => Yii::t('yii', 'Remove'),
                                    'class' => 'grid-action',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to remove this printer?'),
                                    'data-method' => 'delete',
                                    'data-pjax' => '0',
                                    'data-getData' => json_encode(['Printer[external_id]' => $model['id']])
                                ]
                            );
                        }
                        return $a;
                    },



                    'delete' => function ($url, $model, $key) {
                        $a = '';
                        if ($model['status'] == \app\models\Printer::PRINTER_STATUS_INACTIVE) {
                            $a = Html::a('<span class="glyphicon glyphicon-remove-sign"></span>', $url, [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'class' => 'grid-action',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this printer?'),
                                    'data-method' => 'delete',
                                    'data-pjax' => '0',
                                    'data-getData' => json_encode(['Printer[external_id]' => $model['id']])
                                ]
                            );
                        }
                        return $a;
                    }
                ]
            ],
        ],
    ]);
?>
<?php \yii\widgets\Pjax::end(); ?>