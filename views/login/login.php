<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please login with your google account:</p>

    <div class="col-lg-offset-1" style="color:#999;">
        <a href="https://accounts.google.com/o/oauth2/auth?access_type=offline&response_type=code&redirect_uri=<?=\Yii::getAlias('@hostName/login/oauth')?>&scope=email profile https://www.googleapis.com/auth/cloudprint&approval_prompt=force&client_id=<?=\Yii::$app->params['googleApi']['client_id']?>" class="btn btn-success">
            Sign up with Google
        </a>
    </div>
</div>