<?php

use yii\helpers\Html;
use \yii\bootstrap\ActiveForm;
use app\models\Subscription;
use app\models\ClientSubscription;

/* @var $this yii\web\View */
/* @var $clientSubscription app\models\ClientSubscription */
/* @var $subscription app\models\Subscription */

$this->title = 'Edit Subscription: ' . ' ' . $subscription->object_id;
$this->params['breadcrumbs'][] = [
    'label' =>'Cabinet',
    'url' => '/cabinet'
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clientsubscription-update">

    <h1><?= Html::encode($this->title) ?></h1><?php
    $form = ActiveForm::begin(
        [
            'layout' => 'horizontal',

            'id' => 'clientsubscription-update',

            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-sm-1',
                    'wrapper' => 'col-sm-3',
                    'offset' => 'col-sm-offset-0',
                ],
            ],
        ]
    );
    ?>

    <div class="form-group field-subscription-object_id">
        <label class="control-label col-sm-1" for="subscription-object_id">Object ID</label>
        <div class="col-sm-3">
            <span id="subscription-object_id" class="form-control"><?=$subscription->object_id?></span>
        </div>
    </div>

    <div class="form-group field-subscription-type">
        <label class="control-label col-sm-1" for="subscription-type">Type</label>
        <div class="col-sm-3">
            <span id="subscription-type" class="form-control"><?=$subscription->type?></span>
        </div>
    </div>

    <div class="form-group field-clientsubscription-filters">
        <label class="control-label col-sm-1" for="clientsubscription-filters">Filters</label>
        <div class="col-sm-3">
            <?= Html::textarea(
                'ClientSubscription[filters]',
                implode(
                    ', ',
                    array_merge(
                        array_map(
                            function($el) {
                                return '#'.$el;
                            },
                            $clientSubscription->tags
                        ),
                        array_map(
                            function($el) {
                                return '@'.$el;
                            },
                            $clientSubscription->links
                        )
                    )
                ),
                ['id' => 'clientsubscription-filters', 'placeholder' => 'e.g. #tag, @user', 'class' => 'form-control']) ?>
            <div class="help-block help-block-error "></div>
        </div>

    </div>

    <?= $form->field($clientSubscription, 'start_date')->widget(\yii\jui\DatePicker::className(),['options' => ['class' => 'form-control'], 'dateFormat' => 'yyyy-MM-dd','clientOptions' => [ 'defaultDate' => date('Y-m-d')]])?>

    <?= $form->field($clientSubscription, 'end_date')->widget(\yii\jui\DatePicker::className(), ['options' => ['class' => 'form-control'], 'dateFormat' => 'yyyy-MM-dd'])?>

    <div class="form-group">
        <?= Html::submitButton('Create', ['class' => 'btn btn-primary col-sm-offset-3']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
