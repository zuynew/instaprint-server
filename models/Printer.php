<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "printer".
 *
 */
class Printer extends BaseModels\Printer
{
    const PRINTER_STATUS_AVAILABLE = 'available';
    const PRINTER_STATUS_ACTIVE = 'active';
    const PRINTER_STATUS_INACTIVE = 'inactive';
}
