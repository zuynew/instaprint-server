<?php

namespace app\models;

use app\components\behaviors\VerifyToken\IVerifyToken;
use app\components\behaviors\VerifyToken\VerifyToken;
use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "client".
 *
 * @property Subscription[] $subscriptions
 * @property Image[] $images
 */
class Client extends BaseModels\Client implements IVerifyToken, IdentityInterface
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this
            ->hasMany(Subscription::className(), ['id' => 'subscription_id'])
            ->via('clientSubscriptions')
        ;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages() {
        return $this
            ->hasMany(Image::className(), ['id' => 'image_id'])
            ->via('clientImages')
        ;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientSubscriptions()
    {
        return $this->hasMany(ClientSubscription::className(), ['client_id' => 'id']);
    }


    public function behaviors()
    {
        return [
            [
                'class' => VerifyToken::className(),
                'verifyTokenAttribute' => 'api_key',
            ],
        ];
    }

    public function getVerifyToken()
    {
        return $this->api_key;
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['api_key' => $token]);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getAttribute('id');
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function generateAuthKey () {
        $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
    }
}
