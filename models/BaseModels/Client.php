<?php

namespace app\models\BaseModels;

use Yii;

/**
 * This is the model class for table "client".
 *
 * @property integer $id
 * @property string $email
 * @property string $name
 * @property string $api_key
 * @property string $oauth_access_token
 * @property integer $oauth_valid_until
 * @property string $oauth_refresh_token
 * @property string $auth_key
 *
 * @property ClientImage[] $clientImages
 * @property ClientSubscription[] $clientSubscriptions
 * @property Printer[] $printers
 */
class Client extends \app\models\BaseModels\AbstractBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name', 'oauth_access_token', 'oauth_valid_until', 'oauth_refresh_token'], 'required'],
            [['oauth_valid_until'], 'integer'],
            [['email'], 'string', 'max' => 155],
            [['name', 'api_key', 'oauth_access_token', 'oauth_refresh_token', 'auth_key'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['api_key'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'name' => 'Name',
            'api_key' => 'Api Key',
            'oauth_access_token' => 'Oauth Access Token',
            'oauth_valid_until' => 'Oauth Valid Until',
            'oauth_refresh_token' => 'Oauth Refresh Token',
            'auth_key' => 'Auth Key',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientImages()
    {
        return $this->hasMany(ClientImage::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientSubscriptions()
    {
        return $this->hasMany(ClientSubscription::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrinters()
    {
        return $this->hasMany(Printer::className(), ['client_id' => 'id']);
    }
}
