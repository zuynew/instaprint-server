<?php

namespace app\models\BaseModels;

use Yii;

/**
 * This is the model class for table "subscription".
 *
 * @property integer $id
 * @property string $object_id
 * @property string $verify_token
 * @property integer $subscription_external_id
 * @property string $type
 * @property integer $active
 *
 * @property ClientSubscription[] $clientSubscriptions
 * @property Image[] $images
 */
class Subscription extends \app\models\BaseModels\AbstractBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id', 'type'], 'required'],
            [['subscription_external_id', 'active'], 'integer'],
            [['type'], 'string'],
            [['object_id'], 'string', 'max' => 255],
            [['verify_token'], 'string', 'max' => 45],
            [['object_id', 'type'], 'unique', 'targetAttribute' => ['object_id', 'type'], 'message' => 'The combination of Object ID and Type has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'object_id' => 'Object ID',
            'verify_token' => 'Verify Token',
            'subscription_external_id' => 'Subscription External ID',
            'type' => 'Type',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientSubscriptions()
    {
        return $this->hasMany(ClientSubscription::className(), ['subscription_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['subscription_id' => 'id']);
    }
}
