<?php

namespace app\models\BaseModels;

use Yii;

/**
 * This is the model class for table "client_image".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $image_id
 * @property string $watermarked_path
 *
 * @property Client $client
 * @property Image $image
 */
class ClientImage extends \app\models\BaseModels\AbstractBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'image_id'], 'required'],
            [['client_id', 'image_id'], 'integer'],
            [['watermarked_path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'image_id' => 'Image ID',
            'watermarked_path' => 'Watermarked Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Image::className(), ['id' => 'image_id']);
    }
}
