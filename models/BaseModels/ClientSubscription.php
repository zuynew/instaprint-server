<?php

namespace app\models\BaseModels;

use Yii;

/**
 * This is the model class for table "client_subscription".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $subscription_id
 * @property string $start_date
 * @property string $end_date
 * @property string $tags
 * @property string $links
 * @property string $watermark
 *
 * @property Client $client
 * @property Subscription $subscription
 */
class ClientSubscription extends \app\models\BaseModels\AbstractBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'subscription_id'], 'integer'],
            [['start_date'], 'required'],
            [['start_date', 'end_date'], 'safe'],
            [['tags', 'links', 'watermark'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'subscription_id' => 'Subscription ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'tags' => 'Tags',
            'links' => 'Links',
            'watermark' => 'Watermark',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['id' => 'subscription_id']);
    }
}
