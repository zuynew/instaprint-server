<?php

namespace app\models\BaseModels;

use Yii;

/**
 * This is the model class for table "printer".
 *
 * @property integer $id
 * @property string $name
 * @property string $external_id
 * @property integer $client_id
 *
 * @property Client $client
 */
class Printer extends \app\models\BaseModels\AbstractBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'printer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['external_id', 'client_id'], 'required'],
            [['client_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['external_id'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'external_id' => 'External ID',
            'client_id' => 'Client ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }
}
