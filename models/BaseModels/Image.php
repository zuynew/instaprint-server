<?php

namespace app\models\BaseModels;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property string $path
 * @property string $created_at
 * @property integer $subscription_id
 * @property string $username
 * @property string $userpic_path
 *
 * @property ClientImage[] $clientImages
 * @property Subscription $subscription
 */
class Image extends \app\models\BaseModels\AbstractBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path', 'subscription_id', 'username'], 'required'],
            [['created_at'], 'safe'],
            [['subscription_id'], 'integer'],
            [['path', 'userpic_path'], 'string', 'max' => 255],
            [['username'], 'string', 'max' => 150],
            [['path'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'created_at' => 'Created At',
            'subscription_id' => 'Subscription ID',
            'username' => 'Username',
            'userpic_path' => 'Userpic Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientImages()
    {
        return $this->hasMany(ClientImage::className(), ['image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['id' => 'subscription_id']);
    }
}
