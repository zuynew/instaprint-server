<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/4/14
 * Time: 12:08 AM
 */

namespace app\models\BaseModels;


abstract class AbstractBaseModel extends \yii\db\ActiveRecord {

    public function __get($name)
    {
        $getter = 'get' . $name;
        if ($this->hasAttribute($name) && method_exists($this, $getter)) {
            return $this->$getter();
        }
        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if ($this->hasAttribute($name) && method_exists($this, $setter)) {
            $this->$setter($value);
            return;
        }
        parent::__set($name, $value);
    }
} 