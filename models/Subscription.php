<?php

namespace app\models;

use \app\components\behaviors\VerifyToken;
use Yii;

/**
 * This is the model class for table "subscription".
 *
 * @property Client[] $clients
 * @property Client[] $activeClients
 * @property ClientSubscription[] $activeClientSubscriptions
 */
class Subscription extends BaseModels\Subscription implements VerifyToken\IVerifyToken
{

    const SUBSCRIPTION_TYPE_TAG = 'tag';
    const SUBSCRIPTION_TYPE_USER = 'user';
    const SUBSCRIPTION_TYPE_LOCATION = 'location';
    const SUBSCRIPTION_TYPE_GEOGRAPHY = 'geography';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id', 'type'], 'required'],
            [['subscription_external_id'], 'integer'],
            [['active'], 'boolean'],
            [['type'], 'string'],
            [['object_id'], 'string', 'max' => 255],
            [['verify_token'], 'string', 'max' => 45],
            [['object_id', 'type'], 'unique', 'targetAttribute' => ['object_id', 'type'], 'message' => 'The combination of Object ID and Type has already been taken.']
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this
            ->hasMany(Client::className(), ['id' => 'client_id'])
            ->viaTable(ClientSubscription::tableName(), ['subscription_id' => 'id'])
        ;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveClients()
    {
        return $this
            ->hasMany(Client::className(), ['id' => 'client_id'])
            ->viaTable(
                ClientSubscription::tableName(),
                ['subscription_id' => 'id'],
                function ($query)  {
                    $query->andWhere('start_date <= NOW() AND (end_date >= NOW() OR end_date IS NULL)');
                }
            )
        ;
    }
    public function behaviors()
    {
        return [
            [
                'class' => VerifyToken\VerifyToken::className(),
                'verifyTokenAttribute' => 'verify_token',
            ],
        ];
    }

    public function getVerifyToken()
    {
        return $this->verify_token;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveClientSubscriptions()
    {
        return $this
            ->hasMany(ClientSubscription::className(), ['subscription_id' => 'id'])
            ->andWhere('start_date <= NOW() AND (end_date >= NOW() OR end_date IS NULL)')
        ;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientSubscriptions()
    {
        return $this->hasMany(ClientSubscription::className(), ['subscription_id' => 'id']);
    }

}
