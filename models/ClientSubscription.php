<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 7:58 PM
 */

namespace app\models;

/**
 * This is the model class for table "client_subscription".
 *
 * @property Client $client
 * @property Subscription $subscription
 */

class ClientSubscription extends BaseModels\ClientSubscription {

    const WATERMARK_TYPE_UNDERLAY = 'underlay';
    const WATERMARK_TYPE_OVERLAY = 'overlay';

    public function rules()
    {
        return [
            [['client_id', 'subscription_id'], 'integer'],
            [['start_date'], 'required'],
            [['start_date', 'end_date'], 'safe'],
            [['tags', 'links', 'watermark'], 'safe']
        ];
    }


    public function setTags ($tags)
    {
        if (is_array($tags)) {
            $tags = serialize($tags);
        }
        $this->setAttribute('tags', $tags);
    }

    public function getTags ()
    {
        return $this->getAttribute('tags') ? unserialize($this->getAttribute('tags')) : [];
    }

    public function setLinks ($links)
    {
        if (is_array($links)) {
            $links = serialize($links);
        }
        $this->setAttribute('links', $links);
    }

    public function getLinks ()
    {
        return $this->getAttribute('links') ? unserialize($this->getAttribute('links')) : [];
    }


    public function setWatermark ($watermark)
    {
        if (is_array($watermark)) {
            $params = [
                'path' => '@watermarks/watermark.png',
                'type' => static::WATERMARK_TYPE_UNDERLAY, // types: overlay/underlay
                'margins' => [
                    'x' => 0,
                    'y' => 0
                ]
            ];
            $watermark = array_replace_recursive($params, $watermark);
            $watermark = serialize($watermark);
        }
        $this->setAttribute('watermark', $watermark);
    }

    public function getWatermark ()
    {
        return !$this->getAttribute('watermark') ?: unserialize($this->getAttribute('watermark'));
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['id' => 'subscription_id']);
    }
} 