$(document).ready(function(){
    $('body').on('click', '.grid-action', function(e){
        var href = $(this).attr('href');
        var method = ($(this).attr('data-method') === undefined) ? 'get' : $(this).attr('data-method');
        var postData = ($(this).attr('data-postdata') === undefined) ? {} : JSON.parse($(this).attr('data-postdata'));
        var getData = ($(this).attr('data-getdata') === undefined) ? {} : JSON.parse($(this).attr('data-getdata'));

        var self = this;
        $.ajax(
            {
                'url': href+'&'+$.param(getData),
                'method': method,
                'data': postData,
                'success': function (data) {
                    var pjax_id = $(self).closest('.pjax-wraper').attr('id');
                    $.pjax.reload('#' + pjax_id);
                }
            }
        );
        return false;
    })
});