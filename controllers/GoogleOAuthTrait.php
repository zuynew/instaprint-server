<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/8/14
 * Time: 5:50 PM
 */

namespace app\controllers;


use app\models\Client;
use yii\web\HttpException;

trait GoogleOAuthTrait {

    protected function _getGoogleClient() {
        $config = new \Google_Config();

        $config->setClientId(\Yii::$app->params['googleApi']['client_id']);
        $config->setClientSecret(\Yii::$app->params['googleApi']['client_secret']);
        $config->setRedirectUri(\Yii::getAlias('@hostName/login/oauth'));

        $googleClient = new \Google_Client($config);

        $googleClient->setAccessToken(json_encode(['access_token' =>\Yii::$app->getUser()->identity->oauth_access_token]));

        if ($googleClient->isAccessTokenExpired()) {
            $googleClient->refreshToken(\Yii::$app->getUser()->identity->oauth_refresh_token);

            $client = Client::findOne(['id' => \Yii::$app->getUser()->identity->getId()]);

            $respRaw = $googleClient->getAccessToken();
            $resp = json_decode($respRaw, true);

            $client->oauth_access_token = $resp['access_token'];

            if (isset($resp['refresh_token'])) {
                $client->oauth_refresh_token = $resp['refresh_token'];
                $client->oauth_valid_until = $resp['created'] + $resp['expires_in'];
            }

            if (!$client->save()) {
                throw new HttpException(400, 'Can\'t save client');
            }

            $googleClient->setAccessToken($respRaw);
        }

        return $googleClient;

    }

}