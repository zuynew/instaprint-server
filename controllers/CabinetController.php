<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/8/14
 * Time: 4:28 PM
 */

namespace app\controllers;

use app\models\ClientSubscription;
use app\models\Printer;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class CabinetController extends Controller {

    public function actionIndex () {

        $clientSubscriptionProvider = new ActiveDataProvider([
            'query' =>
                ClientSubscription::find()
                    ->with(
                        [
                            'subscription' => function ($query) {
                                $query->orderBy(['object_id' => 'ASC']);
                            }
                        ]
                    )
                    ->where('client_id = :clientId', [':clientId' => \Yii::$app->getUser()->getId()])
            ,
        ]);

        $printerProvider = new ActiveDataProvider([
            'query' =>
                Printer::find()
                    ->where('client_id = :clientId', [':clientId' => \Yii::$app->getUser()->getId()])
            ,
        ]);

        return $this->render('index', [
            'clientSubscriptionProvider' => $clientSubscriptionProvider,
            'printerProvider' => $printerProvider
        ]);

    }
} 