<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/9/14
 * Time: 2:45 AM
 */

namespace app\controllers;

use app\models\ClientSubscription;
use app\models\Subscription;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class SubscriptionController extends Controller {

    use InstagramApiTrait;
    use HookTrait;

    public function actionCreate () {
        if (!\Yii::$app->getRequest()->getIsPost()) {
            throw new HttpException(405);
        }

        if (
            !(
                ($subscriptionPost = \Yii::$app->getRequest()->post('Subscription'))
                &&
                ($clientSubscriptionPost = \Yii::$app->getRequest()->post('ClientSubscription'))
                &&
                isset($subscriptionPost['object_id'])
                &&
                isset($subscriptionPost['type'])
            )
        ) {
            throw new HttpException(400, "Subscription & ClientSubscription data required");
        }

        $transaction = \Yii::$app->db->beginTransaction();

        try {

            $subscription = Subscription::findOne($subscriptionPost);

            $subscribed = true;

            if (!($subscription instanceof Subscription)) {

                $subscription = new Subscription();
                $subscription->load($subscriptionPost, '');

                if (!$subscription->save()) {
                    throw new HttpException(500, 'Can\'t save Subscription');
                }

                $subscribed = false;

                $transaction->commit();

                $transaction = \Yii::$app->db->beginTransaction();

            }

            if (!$subscription->active || !$subscribed) {

                $api = $this->_getInstapi();

                $data = $api->createSubscription(
                    [
                        'object' => $subscription->type,
                        'object_id' => $subscription->object_id,
                        'verify_token' => $subscription->verify_token,
                        'callback_url' => $this->_getHookCallbackUrl($subscription)]
                    ,
                    []
                );

                $subscription->subscription_external_id = $data['id'];
                $subscription->active = true;

                if (!$subscription->save()) {
                    throw new HttpException(500, 'Can\'t save Subscription');
                }
            }

            $clientSubscription = new ClientSubscription();

            $clientSubscription->load($clientSubscriptionPost, '');

            if (isset($clientSubscriptionPost['filters']) && !empty($clientSubscriptionPost['filters'])) {
                $filters = explode(',', $clientSubscriptionPost['filters']);

                $tags = [];
                $links = [];

                foreach($filters as $filter) {
                    $item = trim($filter);

                    if (strpos($item, '#') === 0) {
                        $tags[] = substr($item, 1);
                    }

                    if (strpos($item, '@') === 0) {
                        $links[] = substr($item, 1);
                    }
                }

                $clientSubscription->tags = $tags;
                $clientSubscription->links = $links;

            }

            $clientSubscription->client_id = \Yii::$app->getUser()->getId();
            $clientSubscription->subscription_id = $subscription->id;

            if (!$clientSubscription->save()) {
                throw new HttpException(500, 'Can\'t save ClientSubscription');
            }


        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        $transaction->commit();

    }

    public function actionDelete () {
        if (!($clientSubscriptionId = \Yii::$app->getRequest()->get('id'))) {
            throw new HttpException(400, 'ClientSubscriptionId required');
        }

        $clientSubscription = ClientSubscription::findOne(['id' => $clientSubscriptionId, 'client_id' => \Yii::$app->getUser()->identity->getId()]);

        if (!($clientSubscription instanceof ClientSubscription)) {
            throw new HttpException(404, 'ClientSubscription Not Found');
        }

        $transaction = \Yii::$app->db->beginTransaction();

        try {

            $clientSubscription->end_date = date('Y-m-d H:i:s');

            if (!$clientSubscription->save()) {
                throw new HttpException(500, 'Can\'t save ClientSubscription');
            }

            $subscription = $clientSubscription->subscription;

            $activeClients = $subscription->activeClients;

            if (count($activeClients) == 0) {
                $api = $this->_getInstapi();

                $data = $api->deleteSubscription(
                    [
                        'id' => $subscription->subscription_external_id
                    ],
                    []
                );

                $subscription->active = 0;

                if (!$subscription->save()) {
                    throw new HttpException(500, 'Can\'t save Subscription');
                }

            }


        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        $transaction->commit();

    }

    public function actionEdit ($id) {

        $clientSubscription = ClientSubscription::findOne(['id' => $id, 'client_id' => \Yii::$app->getUser()->identity->getId()]);

        if (!($clientSubscription instanceof ClientSubscription)) {
            throw new NotFoundHttpException();
        }

        if (\Yii::$app->getRequest()->isPost) {
            if (!($clientSubscriptionPost = \Yii::$app->getRequest()->post('ClientSubscription')))
            {
                throw new BadRequestHttpException('ClientSubscription Required');
            }

            $tags = [];

            $links = [];

            if (isset($clientSubscriptionPost['filters']))
            {

                $filters = explode(',' ,$clientSubscriptionPost['filters']);

                array_walk(
                    $filters,
                    function ($el) use (&$tags, &$links)
                    {
                        $el = trim($el);
                        $type = substr($el, 0, 1);
                        $item = substr($el, 1);

                        switch ($type) {
                            case '#':
                                $tags[] = $item;
                                break;
                            case '@':
                                $links[] = $item;
                                break;
                        }

                    }
                );
            }

            $clientSubscription->load($clientSubscriptionPost, '');

            $clientSubscription->tags = $tags;
            $clientSubscription->links = $links;

            if (!$clientSubscription->save())
            {
                throw new HttpException(500, "ClientSubscription save fail");
            }

            $this->redirect('/cabinet');

        }

        return $this->render(
            'edit',
            [
                'clientSubscription' => $clientSubscription,
                'subscription' => $clientSubscription->subscription,
            ]
        );


    }

} 