<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/9/14
 * Time: 2:56 PM
 */

namespace app\controllers;


use zuynew\instapi\Instapi;
use zuynew\instapi\Method\DefaultMethodFactory;

trait InstagramApiTrait {

    protected function _getInstapi() {

        $api = new Instapi(
            new \zuynew\instapi\Client\Client(
                \Yii::$app->params['instgramApi']['client_id'],
                \Yii::$app->params['instgramApi']['client_secret']
            ),
            new DefaultMethodFactory()
        );

        return $api;

    }

} 