<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 10/28/14
 * Time: 2:02 PM
 */

namespace app\controllers;

use app\models;
use yii\rest;
use yii\web\NotFoundHttpException;

class HookController extends rest\Controller {

    use HookTrait;

    public function behaviors() {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'tag'  => ['get', 'post'],
                ]
            ]
        ];
    }

    public function actionTag ($tagName) {

        $subscription = models\Subscription::findOne(['object_id' => $tagName, 'type' => models\Subscription::SUBSCRIPTION_TYPE_TAG]);

        if (!($subscription instanceof models\Subscription)) {
            throw new NotFoundHttpException('Tag ('.$tagName.') subscription not found');
        }

        if (\Yii::$app->getRequest()->isPost) {
//            $this->_runUpdate();
        } else {
            $this->_verifyHook($subscription);
        }

        $this->_sendAck();

    }

} 