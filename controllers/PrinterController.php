<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/8/14
 * Time: 6:45 PM
 */

namespace app\controllers;

use app\models\Printer;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\HttpException;

class PrinterController extends Controller {
    use GoogleOAuthTrait;

    public function actionListPrinters () {

        $googleClient = $this->_getGoogleClient();

        $api =  new \GoogleCloudPrint();

        $api->setAccessToken(\Yii::$app->getUser()->identity->oauth_access_token);

        $availablePrinters = $api->getPrinters();

        $printerIds = [];

        foreach ($availablePrinters as $i => $printer) {
            $printerIds[$printer['id']] = $i;
        }

        $activePrinters = Printer::findAll(['external_id' => array_keys($printerIds), 'client_id' =>\Yii::$app->getUser()->getId()]);

        $printers = [];

        foreach ($activePrinters as $printer) {
            unset($availablePrinters[$printerIds[$printer->external_id]]);
            $printers[$printer->external_id] = [
                'name' => $printer->name,
                'id' => $printer->external_id,
                'status' => Printer::PRINTER_STATUS_ACTIVE
            ];
        }

        $inactivePrinters = Printer::find()
            ->where(['client_id' => \Yii::$app->getUser()->getId()])
            ->andWhere(['not in', 'external_id',array_keys( $printerIds)])
            ->all()
        ;

        foreach ($inactivePrinters as $printer) {
            $printers[$printer->external_id] = [
                'name' => $printer->name,
                'id' => $printer->external_id,
                'status' => Printer::PRINTER_STATUS_INACTIVE
            ];
        }

        foreach ($availablePrinters as $printer) {
            $printers[$printer['id']] = [
                'name' => $printer['name'],
                'id' => $printer['id'],
                'status' => Printer::PRINTER_STATUS_AVAILABLE
            ];
        }


        usort($printers, function ($a, $b) {
                return strcmp($a['name'], $b['name']);
            }
        );

        $printerProvider = new ArrayDataProvider([
            'allModels' => $printers,
//            'sort' => [
//                'attributes' => ['id', 'name'],
//            ]
        ]);

        return $this->render('list-printers', [
                'printerProvider' => $printerProvider
            ]
        );

    }

    public function actionAppend () {
        if (!\Yii::$app->getRequest()->getIsPost()) {
            throw new HttpException(405);
        }

        if (!($printerPost = \Yii::$app->getRequest()->post('Printer')) && isset($printerPost['external_id'])) {
            throw new HttpException(400, 'Printer data required');
        }

        $printerPost = array_merge(['client_id' => \Yii::$app->getUser()->identity->getId()], $printerPost);

        $printer = Printer::findOne($printerPost);

        if ($printer instanceof Printer) {
            throw new HttpException(400, 'Printer Already Appended');
        }

        $printer = new Printer();

        $printer->load($printerPost, '');
        $printer->client_id = \Yii::$app->getUser()->identity->getId();


        if (!$printer->save()) {

            throw new HttpException(500, 'Can\'t save printer');
        }

        \Yii::$app->end(200);

    }

    public function actionRemove () {

        if (!\Yii::$app->getRequest()->getIsDelete()) {
            throw new HttpException(405);
        }

        if (!($printerGet = \Yii::$app->getRequest()->get('Printer')) && isset($printerGet['external_id'])) {
            throw new HttpException(400, 'Printer data required');
        }

        $printerGet = array_merge(['client_id' => \Yii::$app->getUser()->identity->getId()], $printerGet);

        $printer = Printer::findOne($printerGet);

        if (!($printer instanceof Printer)) {
            throw new HttpException(400, 'Printer doesn\'t exist');
        }


        if (!$printer->delete()) {

            throw new HttpException(500, 'Can\'t delete printer');
        }

        \Yii::$app->end(204);

    }

} 