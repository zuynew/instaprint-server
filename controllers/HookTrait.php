<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 7:08 PM
 */

namespace app\controllers;

use app\components\behaviors\VerifyToken\IVerifyToken;
use app\components\Job\DeferredJob;
use app\components\Job\Job;
use app\exceptions\HookVerificationException;
use app\models\Subscription;


/**
 * Class HookTrait
 * @package app\controllers
 *      ENUM('tag', 'user', 'location', 'geography')
 */
trait HookTrait {

    protected function _verifyHook (IVerifyToken $model)
    {
        $request = \Yii::$app->getRequest();
        if (
            !(
                $request->get('hub_challenge')
                &&
                (
                    $model->getVerifyToken() == $request->get('hub_verify_token')
                )
            )
        ) {
            throw new HookVerificationException();
        }
    }

    protected function _getHookData ()
    {
        $body = \Yii::$app->getRequest()->rawBody;
        return json_decode($body, true);
    }

    protected function _runUpdate ()
    {
       $data = $this->_getHookData()[0];

       DeferredJob::work('update', 'index', ['subscribeExternalId' => $data['subscription_id'], 'minTimestamp' => $data['time']]);
    }

    protected function _sendAck ()
    {
        echo \Yii::$app->getRequest()->get('hub_challenge');
        \Yii::$app->end();
    }

    protected function _getHookCallbackUrl () {
        if (func_num_args() == 1) {
            /** @var Subscription $model */
            $model = func_get_arg(0);
            if (!($model instanceof Subscription)) {
                throw new \InvalidArgumentException();
            }
            $type = $model->type;
            $object_id = $model->object_id;
        } elseif (func_num_args() == 2) {
            list($object_id, $type) = func_get_args();
        } else {
            throw new \InvalidArgumentException();
        }

        return \Yii::getAlias('@hookPath/'.$type.'/'.$object_id);

    }

} 