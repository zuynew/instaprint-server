<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/5/14
 * Time: 1:20 PM
 */

namespace app\controllers;


use app\models\Client;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\HttpException;

class LoginController extends Controller {


    public function actionLogin () {
        return $this->render('login');
    }

    public function actionOauth () {
        if (!$code = \Yii::$app->getRequest()->get('code')) {
            throw new BadRequestHttpException('code required');
        }

        $config = new \Google_Config();

        $config->setClientId(\Yii::$app->params['googleApi']['client_id']);
        $config->setClientSecret(\Yii::$app->params['googleApi']['client_secret']);
        $config->setRedirectUri(\Yii::getAlias('@hostName/login/oauth'));


        $googleClient = new \Google_Client($config);

        $resp = json_decode($googleClient->authenticate($code), true);

        $aouth = new \Google_Service_Oauth2($googleClient);

        $profile = $aouth->userinfo->get();

        $client = Client::findOne(['email' => $profile->email]);

        if (!($client instanceof Client)) {

            $client = new Client();

            $client->email = $profile['email'];
            $client->name = $profile['given_name'].' '.$profile['family_name'];
            $client->oauth_access_token = $resp['access_token'];
            $client->generateAuthKey();

            if (isset($resp['refresh_token'])) {
                $client->oauth_refresh_token = $resp['refresh_token'];
                $client->oauth_valid_until = $resp['created'] + $resp['expires_in'];
            }

            if (!$client->save()) {
                throw new HttpException(400, 'Can\'t save client');
            }
        }

        \Yii::$app->user->login($client, 3600*24*30);

        return $this->redirect('/cabinet');

    }

} 