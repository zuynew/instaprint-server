<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/5/14
 * Time: 3:54 PM
 */

namespace app\exceptions;


use yii\web\BadRequestHttpException;

class GoogleOauthException extends BadRequestHttpException {

} 