<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\ClientSubscription;
use Imagine\Draw\DrawerInterface;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo \Yii::getAlias('@fonts/ProximaNova-Bold.ttf');
//        $imagine = new Imagine();
//
//        $thumb = $imagine->open('http://photos-f.ak.instagram.com/hphotos-ak-xaf1/10707057_1469196383346085_1986823942_a.jpg');
//        $thumb->resize(new Box(60, 60));
//
//
//        $header = $imagine->create(new Box(640, 60), (new RGB())->color('#ccc', 100));
//
//        $boldFont = $imagine->font('/var/www/instaprint/etc/fonts/ProximaNova-Bold.ttf', 26, (new RGB())->color('#3F729B', 100));
//        $header->draw()->text('nimpletnimpletni', $boldFont, new Point(70, 0));
//
//        $regularFont = $imagine->font('/var/www/instaprint/etc/fonts/ProximaNova-Reg.ttf', 24, (new RGB())->color('#81868A', 100));
//        $header->draw()->text((new \DateTime())->format('d.m.Y'), $regularFont, new Point(475, 0));
//
//        $header->paste($thumb, new Point(0,0));
//
//        $header->save('/var/www/instaprint/uploads/header.png');

    }
}
