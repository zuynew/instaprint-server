<?php

namespace app\commands;

use app\models\Client;
use app\models\ClientSubscription;
use app\models\Printer;
use app\models\Subscription;
use yii\console\Controller;
use zuynew\instapi\Instapi;
use zuynew\instapi\Method\DefaultMethodFactory;

class CrudController extends Controller
{
    public function actionCreateClient ($name)
    {
        $client = new Client();
        $client->name = $name;
        $client->save();

        echo "Client: [".$client->id."] ".$client->name."\n";
    }

    public function actionListClients ()
    {
        $clients = Client::find()->all();

        /** @var Client $client */
        foreach ($clients as $client) {
            echo "[".$client->id."] ".$client->name."\n";
            $clientSubscriptions = $client->clientSubscriptions;
            if (count($clientSubscriptions) > 0) {
                echo "  Subscriptions:\n";
                foreach ($clientSubscriptions as $clientSubscription) {
                    $subscription = $clientSubscription->subscription;
                    echo "    [".$subscription->id."] ".$subscription->object_id." (".$subscription->type.") ";
                    echo "   ".$clientSubscription->start_date."    ";
                    echo $clientSubscription->end_date."    ";
                    echo "tags: [".implode(', ', $clientSubscription->tags)."]     ";
                    echo "links: [".implode(', ', $clientSubscription->links)."] ";
                    echo !$subscription->active ? '[inactive]' : '';
                    echo "\n";
                }
            }
            $printers = $client->printers;
            if (count($printers) > 0) {
                echo "  Printers:\n";
                foreach ($printers as $printer) {
                    echo "    [".$printer->id."] ".$printer->name." (".$printer->external_id.") \n";
                }
            }
        }
    }

    public function actionCreateSubscription ($object_id, $type)
    {
        $subscription = new Subscription();
        $subscription->object_id = $object_id;
        $subscription->type = $type;

        if (!$subscription->save()) {
            throw new \RuntimeException('Can\'t save subscription');
        }

        $api = new Instapi(
            new \zuynew\instapi\Client\Client(
                \Yii::$app->params['instgramApi']['client_id'],
                \Yii::$app->params['instgramApi']['client_secret']
            ),
            new DefaultMethodFactory()
        );

        $data = $api->createSubscription(['object' => $type, 'object_id' => $object_id, 'verify_token' => $subscription->verify_token, 'callback_url' => \Yii::getAlias('@hookPath/'.$type.'/'.$object_id)], []);

        $subscription->subscription_external_id = $data['id'];


        if (!$subscription->save()) {
            throw new \RuntimeException('Can\'t update subscription');
        }

        echo "Subscription: [".$subscription->id."] ".$subscription->object_id."(".$subscription->type.")\n";
    }

    public function actionListSubscriptions ()
    {
        $subscriptions = Subscription::find()->all();

        /** @var Subscription $subscription */
        foreach ($subscriptions as $subscription) {
            echo "[".$subscription->id."] ".$subscription->object_id." (".$subscription->type.") \n";
        }
    }

    public function actionDeleteSubscription ($object_id, $type)
    {
        $subscription = Subscription::findOne(['object_id' => $object_id, 'type' => $type]);

        if (!($subscription instanceof Subscription)) {
            throw new \RuntimeException('Subscription not found');
        }

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $api = new Instapi(
                new \zuynew\instapi\Client\Client(
                    \Yii::$app->params['instgramApi']['client_id'],
                    \Yii::$app->params['instgramApi']['client_secret']
                ),
                new DefaultMethodFactory()
            );

            $data = $api->deleteSubscription(['id' => $subscription->subscription_external_id]);

            $subscription->active = false;
            if (!$subscription->save()) {
                throw new \RuntimeException('Can\'t update subscription');
            }

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        $transaction->commit();

    }

    public function actionListPrinters ()
    {
        $api = new \GoogleCloudPrint();

        if (
            !$api->loginToGoogle(
                \Yii::$app->params['googleApi']['username'],
                \Yii::$app->params['googleApi']['password']
            )
        ) {
            throw new \RuntimeException('Google login fail');
        }

        $printers = $api->getPrinters();

        foreach ($printers as $i => $printer) {
            echo "[".$i."] ".$printer['name']." ".$printer['id']."\n";
        }

    }



    public function actionAppendPrinter ($clientId, $printerIndex)
    {
        $client = Client::findOne(['id' => $clientId]);

        if (!($client instanceof Client)) {
            throw new \RuntimeException('Client not found');
        }

        $printer = new Printer();

        $api = new \GoogleCloudPrint();

        if (
        !$api->loginToGoogle(
            \Yii::$app->params['googleApi']['username'],
            \Yii::$app->params['googleApi']['password']
        )
        ) {
            throw new \RuntimeException('Google login fail');
        }

        $printers = $api->getPrinters();

        $printer->name = $printers[$printerIndex]['name'];
        $printer->external_id = $printers[$printerIndex]['id'];
        $printer->client_id = $client->id;

        if (!$printer->save()) {
            throw new \RuntimeException('Can\'t update printer');
        }

    }


    public function actionDeletePrinter ($clientId, $printerId)
    {
        $client = Client::findOne(['id' => $clientId]);

        if (!($client instanceof Client)) {
            throw new \RuntimeException('Client not found');
        }

        $printer = Printer::findOne(['id' => $printerId]);

        if (!($printer instanceof Printer)) {
            throw new \RuntimeException('Printer not found');
        }


        if (!$printer->delete()) {
            throw new \RuntimeException('Can\'t delete printer');
        }

    }

    public function actionAppendSubscription ($clientId, $subscriptionId, $startDate='1970-01-01', $endDate=null, $tags = [], $links = [], $watermark = null)
    {
        if (!is_array($tags)) {
            $tags = explode(",", $tags);
        }

        if (!is_array($links)) {
            $links = explode(",", $links);
        }

        if (!is_null($watermark) && !is_array($watermark)) {
            $watermarkParamsTmp = explode(';', $watermark);
            $watermarkParams = [];
            foreach ($watermarkParamsTmp as $watermarkParam) {
                list($key, $value) = explode(':', $watermarkParam);
                if ($key == 'margins') {
                    $valueTmp = explode(',', $value);
                    $value = ['x' => $valueTmp[0], 'y' => $valueTmp[1]];
                }
                $watermarkParams[$key] = $value;
            }
            $watermark = $watermarkParams;
        }

        $client = Client::findOne(['id' => $clientId]);

        if (!($client instanceof Client)) {
            throw new \RuntimeException('Client not found');
        }


        $subscription = Subscription::findOne(['id' => $subscriptionId]);

        if (!($subscription instanceof Subscription)) {
            throw new \RuntimeException('Subscription not found');
        }

        $clientSubscription = new ClientSubscription();

        $clientSubscription->client_id = $client->id;
        $clientSubscription->subscription_id = $subscription->id;

        $clientSubscription->tags = $tags;
        $clientSubscription->links = $links;

        $clientSubscription->watermark = $watermark;

        $clientSubscription->start_date = (new \DateTime($startDate))->format('c');
        $clientSubscription->end_date = $endDate ? (new \DateTime($endDate))->format('c') : null;

        if (!$clientSubscription->save()) {
            throw new \RuntimeException('Can\'t update client');
        }

    }

}
