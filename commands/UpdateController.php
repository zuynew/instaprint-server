<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 8:42 PM
 */

namespace app\commands;


use app\components\Job\DeferredJob;
use app\models\ClientImage;
use app\models\ClientSubscription;
use app\models\Image;
use app\models\Subscription;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;
use yii\console\Controller;
use yii\web\NotFoundHttpException;
use zuynew\instapi\Client\Client;
use zuynew\instapi\Instapi;
use zuynew\instapi\Method\DefaultMethodFactory;

class UpdateController extends Controller {

    public function actionIndex ($subscribeExternalId, $minTimestamp=0, $count=100)
    {

        $subscription = Subscription::findOne(['subscription_external_id' => $subscribeExternalId]);

        if (!($subscription instanceof Subscription)) {
            throw new \RuntimeException('Subscription (ext_id: '.$subscribeExternalId.') not found');
        }
        $api = new Instapi(
            new Client(
                \Yii::$app->params['instgramApi']['client_id'],
                \Yii::$app->params['instgramApi']['client_secret']
            ),
            new DefaultMethodFactory()
        );

        $methodName = 'listMedia'.ucfirst($subscription->type);

        $items = $api->$methodName(['MIN_TIMESTAMP' => $minTimestamp, 'COUNT' => $count], ['object_id' => $subscription->object_id]);

        $res = [];

        if ($subscription->type == Subscription::SUBSCRIPTION_TYPE_TAG) {
            foreach ($items as $item) {
                if ($item['type'] != 'image' || $item['created_time'] < $minTimestamp) {
                    continue;
                }
                $res[] = $item;
            }
        } else {
            $res = $items;
        }
        foreach ($res as $item) {
            $links = [];
            if (isset($item['caption']['text'])) {
                preg_match_all('~\@(\w+)~', $item['caption']['text'], $userLinksTmp);
                $links = isset($userLinksTmp[1]) ? $userLinksTmp[1] : [];
            }
            DeferredJob::work(
                'update',
                'save',
                [
                    'subscriptionId' => $subscription->id,
                    'url' => $item['images']['standard_resolution']['url'],
                    'username' => $item['user']['username'],
                    'userpicUrl' => $item['user']['profile_picture'],
                    'tags' => isset($item['tags']) ? $item['tags'] : [],
                    'links' => $links
                ]
            );
        }

        return 0;

    }

    public function actionSave ($subscriptionId, $url, $username, $userpicUrl, $tags = [], $links = [])
    {
        if (!is_array($tags)) {
            $tags = unserialize(trim($tags, '"'));
        }
        if (!is_array($links)) {
            $links = unserialize(trim($links, '"'));
        }

        $path = '@originalImageUploads/'.md5($url).'.jpg';
        $userpicPath = '@userpicImageUploads/'.md5($userpicUrl).'.jpg';

        $transaction = \Yii::$app->db->beginTransaction();

        try {

            $image = new Image();
            $image->subscription_id = $subscriptionId;
            $image->username = $username;
            $image->path = $path;
            $image->userpic_path = $userpicPath;

            if (!$image->save())
            {
                throw new \RuntimeException("Image save fail (path: ".$path."; url: ".$url.";");
            }

            $cmd = 'wget -O '.\Yii::getAlias($userpicPath).' '.$userpicUrl.' > /dev/null 2>&1';

            exec($cmd, $output, $result);

            if ($result !== 0)
            {
                throw new \Exception("Downloading image (".$userpicUrl.") fail. Output: " . join(PHP_EOL, $output));
            }

            $printJobs = [];
            foreach ($image->subscription->activeClientSubscriptions as $clientSubscription) {
                if (
                    !(
                        count(array_intersect($clientSubscription->tags, $tags)) == count($clientSubscription->tags)
                        &&
                        count(array_intersect($clientSubscription->links, $links)) == count($clientSubscription->links)
                    )
                ) {
                    continue;
                }
                $clientImage = new ClientImage();
                $clientImage->client_id = $clientSubscription->client_id;
                $clientImage->image_id = $image->id;

                if (!$clientImage->save())
                {
                    throw new \RuntimeException("ClientImage save fail");
                }

                $printJobs[] = [
                    'clientSubscriptionId' => $clientSubscription->id,
                    'clientImageId' => $clientImage->id
                ];
            }

            $cmd = 'wget -O '.\Yii::getAlias($path).' '.$url.' > /dev/null 2>&1';

            exec($cmd, $output, $result);

            if ($result !== 0)
            {
                throw new \Exception("Downloading image (".$url.") fail. Output: " . join(PHP_EOL, $output));
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            if (file_exists(\Yii::getAlias($path))) {
                unlink(\Yii::getAlias($path));
            }
            if (file_exists(\Yii::getAlias($userpicPath))) {
                unlink(\Yii::getAlias($userpicPath));
            }
            throw $e;
        }

        $transaction->commit();

        foreach ($printJobs as $printJob) {
            DeferredJob::work('update', 'print', $printJob);
        }

    }

    public function actionPrint($clientSubscriptionId, $clientImageId)
    {
        $clientSubscription = ClientSubscription::findOne(['id' => $clientSubscriptionId]);

        if (!($clientSubscription instanceof ClientSubscription)) {
            throw new \RuntimeException('ClientSubscription not found');
        }

        $clientImage = ClientImage::findOne(['id' => $clientImageId]);

        if (!($clientImage instanceof ClientImage)) {
            throw new \RuntimeException('ClientImage not found');
        }

        $watermarkedImage = $this->_makeWatermark($clientImage->image->path, $clientSubscription->watermark, ['username' => $clientImage->image->username, 'userpicPath' => $clientImage->image->userpic_path]);

        $watermarkedPath = '@watermarkedImageUploads/'.md5($clientImage->image->path.$clientImageId).'.jpg';

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            $watermarkedImage->save(\Yii::getAlias($watermarkedPath), ['jpeg_quality' => 100]);

            $clientImage->watermarked_path = $watermarkedPath;

            if (!$clientImage->save()) {
                throw new \RuntimeException('Can\'t save clientImage');
            }

        } catch (\Exception $e) {
            $transaction->rollBack();
            if (file_exists(\Yii::getAlias($watermarkedPath))) {
                unlink(\Yii::getAlias($watermarkedPath));
            }
            throw $e;
        }
        $transaction->commit();


        $api = new \GoogleCloudPrint();

        if (
            !$api->loginToGoogle(
                \Yii::$app->params['googleApi']['username'],
                \Yii::$app->params['googleApi']['password']
            )
        ) {
            throw new \RuntimeException('Google login fail');
        }

        foreach ($clientSubscription->client->printers as $printer) {
            if (
                !$status = $api->sendPrintToPrinter($printer->external_id, 'Image #'.$clientImage->image->id, \Yii::getAlias($clientImage->watermarked_path), mime_content_type(\Yii::getAlias($clientImage->watermarked_path)))
            ) {
                \Yii::error('Print ('.$printer->external_id.') image ('.$clientImage->image->id.')');
            }
        }
    }

    protected function _makeWatermark ($originalPath, $watermarkConf, $headerConf = null)
    {
        if (!file_exists(\Yii::getAlias($originalPath))) {
            throw new \RuntimeException('File '.$originalPath.' not found');
        }

        if (!file_exists(\Yii::getAlias($watermarkConf['path']))) {
            throw new \RuntimeException('File '.$watermarkConf['path'].' not found');
        }

        $imagine = new Imagine();

        if (is_array($headerConf)) {
            $header =$this->_makeHeader($headerConf['username'], $headerConf['userpicPath']);

            $originTmp = $imagine->open(\Yii::getAlias($originalPath));

            $result = $imagine->create(new Box(640,720));
            $result->paste($header, new Point(0,0));
            $result->paste($originTmp, new Point(0,80));

            $origin = $result;

        } else {
            $origin = $imagine->open(\Yii::getAlias($originalPath));
        }

        if ($watermarkConf['type'] == ClientSubscription::WATERMARK_TYPE_OVERLAY) {
            $underlay = $origin;
            $overlay = $imagine->open(\Yii::getAlias($watermarkConf['path']));
        } else {
            $underlay =$imagine->open(\Yii::getAlias($watermarkConf['path']));
            $overlay = $origin;
        }

        $point = new Point($watermarkConf['margins']['x'], $watermarkConf['margins']['y']);

        return $underlay->paste($overlay, $point);

    }

    protected function _makeHeader ($username, $userpicPath)
    {
        $imagine = new Imagine();

        $thumb = $imagine->open(\Yii::getAlias($userpicPath));
        $thumb->resize(new Box(60, 60));


        $header = $imagine->create(new Box(640, 60), (new RGB())->color('#ccc', 100));

        $boldFont = $imagine->font(\Yii::getAlias('@fonts/ProximaNova-Bold.ttf'), 26, (new RGB())->color('#3F729B', 100));
        $header->draw()->text($username, $boldFont, new Point(70, 0));

        $regularFont = $imagine->font(\Yii::getAlias('@fonts/ProximaNova-Reg.ttf'), 22, (new RGB())->color('#81868A', 100));
        $header->draw()->text((new \DateTime())->format('d.m.Y'), $regularFont, new Point(475, 0));

        $header->paste($thumb, new Point(0,0));

        return $header;
    }

}