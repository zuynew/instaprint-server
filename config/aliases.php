<?php

return array_merge(
    [
        '@app/models/BaseModels' => '@app/models/BaseModels',
        '@app/command' => '@app/bin/command',

        '@uploads' => '@app/uploads',
        '@originalImageUploads' => '@uploads/images/original',
        '@watermarkedImageUploads' => '@uploads/images/watermarked',
        '@watermarks' => '@uploads/images/watermarks',
        '@userpicImageUploads' => '@uploads/images/userpics',

        '@hostName' => 'http://instaprinter.msk-bx.ru',
        '@uploadsUrl' => 'http://uploads.instaprinter.msk-bx.ru',

        '@uploadsOriginalImage' => '@uploadsUrl/images/original',
        '@hookPath' => '@hostName/hook',

        '@etc' => '@app/etc',
        '@fonts' => '@etc/fonts'
    ],
    include 'env_aliases.php'
);