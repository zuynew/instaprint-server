<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=instaprint',
    'username' => 'yii-user',
    'password' => 'root',
    'charset' => 'utf8',
];
