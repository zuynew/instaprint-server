<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Tz6JGT3fadutJGE0T9DQi3JGXCVqRjvj',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Client',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [

                '/' => 'site/index',

                'login' => 'login/login',
                'login/oauth' => 'login/oauth',

                'hook/tag/<tagName:\w+>'=>'hook/tag',

                'cabinet' => 'cabinet/index',
                'cabinet/printers' => 'printer/list-printers',

                'printer/append' => 'printer/append',
                'printer/remove' => 'printer/remove',
                'printer/delete' => 'printer/remove',

                'subscription/create' => 'subscription/create',
                'subscription/edit/<id:\d+>' => 'subscription/edit',
                'subscription/delete' => 'subscription/delete',



                'client' => 'client/index',
                'client/update' => 'client/update'


            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'aliases' => require(__DIR__ . '/aliases.php'),
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1'],
    ];
}

return $config;
