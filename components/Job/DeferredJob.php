<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 8:38 PM
 */

namespace app\components\Job;


class DeferredJob extends AbstractJob {
    static protected function _buildCommand($controllerName, $action = 'index', $params = [])
    {
        return parent::_buildCommand($controllerName, $action, $params).'  >/dev/null 2>&1 & ';
    }
} 