<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 8:22 PM
 */

namespace app\components\Job;


use yii\console\Controller;

interface IJob {
    static public function work($controllerName, $action='index', $params = []);
} 