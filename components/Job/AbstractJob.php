<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/4/14
 * Time: 1:14 AM
 */

namespace app\components\Job;

abstract class AbstractJob implements IJob {

    static protected function _buildCommand ($controllerName, $action='index', $params = [])
    {
        $command = \Yii::getAlias('@app/command');

        $cmd = $command.' '.lcfirst($controllerName).'/'.lcfirst($action);

        foreach ($params as $value) {
            if (is_array($value) || is_object($value)) {
                $value = '"'.addslashes(serialize($value)).'"';
            }
            $cmd .= ' '.$value;
        }

        return $cmd;

    }

    static public function work($controllerName, $action='index', $params = [])
    {
        $cmd = static::_buildCommand($controllerName, $action, $params);

        exec($cmd, $output, $result);

        if ($result !== 0)
        {
            throw new \Exception("Couldn't complete job. Result was: '$result'. Output: " . join(PHP_EOL, $output));
        }
    }

}