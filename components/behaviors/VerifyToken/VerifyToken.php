<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 7:01 PM
 */


namespace app\components\behaviors\VerifyToken;


use yii\base\Behavior;
use yii\db\ActiveRecord;

class VerifyToken extends Behavior
{

    public $verifyTokenAttribute = 'verify_token';

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'createVerifyToken'
        ];
    }

    public function createVerifyToken($event)
    {
        $this->owner->{$this->verifyTokenAttribute} = md5(time().mt_rand(0,999));
    }
}