<?php
/**
 * Created by PhpStorm.
 * User: zuynew
 * Date: 11/2/14
 * Time: 7:21 PM
 */

namespace app\components\behaviors\VerifyToken;


interface IVerifyToken {

    public function getVerifyToken();

} 